const fs = require("fs");

function callback2(boardId, callBack){
    setTimeout(() => {
        fs.readFile('./lists.json', 'utf-8', (err, allListsBasedOnBoardId) => {
            if(err) {
                callBack(err, "Unable to read file");
            } else {
                const listBasedOnBoardId =  Object.entries(JSON.parse(allListsBasedOnBoardId)).filter(([id, lists]) => {
                    return id === boardId;
                });

                callBack(null, Object.fromEntries(listBasedOnBoardId));
            }
        });
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
}

module.exports = callback2;