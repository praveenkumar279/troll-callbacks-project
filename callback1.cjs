const fs = require("fs");

function callback1(boardId, callBack){
    setTimeout(() => {
        fs.readFile('./boards.json', 'utf-8', (err, boardsData) => {
            if(err) {
                callBack(err, "Error");
            } else {
                const boardsDataByID = JSON.parse(boardsData).filter((board) => {
                    return board.id === boardId;
                })
    
                callBack(null, boardsDataByID);
            }
        });
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
}

module.exports = callback1;