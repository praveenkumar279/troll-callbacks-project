const fs = require("fs");

function callback3(listId, callBack){
    setTimeout(() => {
        fs.readFile('./cards.json', 'utf-8', (err, cardsData) => {
            if(err) {
                callBack(err, "Unable to read file");
            } else {
                const cardsByListID = Object.entries(JSON.parse(cardsData)).filter(([id, cards]) => {
                    return id === listId;
                });

                callBack(null, Object.fromEntries(cardsByListID));
            }
        });
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
}

module.exports = callback3;