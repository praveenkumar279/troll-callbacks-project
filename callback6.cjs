const fs = require("fs");

const boardsData = require('./boards.json')

const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

function callback6(){
    setTimeout(() => {
        let board = boardsData.filter((board) => {
            return board.name === "Thanos";
        }).map((thanos) => {
            return thanos.id;
        });
    
        let boardId = board[0];
    
        // Get information from thanos board by calling callback1 function
    
        callback1(boardId, (err, boardData) => {
            if(err) {
                console.error(err);
            } else {
                console.log(boardData);

                // Get all the lists for the thanos board by calling callback2 function
    
                callback2(boardId, (err, listsData) => {
                    if(err) {
                        console.error(err);
                    } else {
                        console.log(listsData);
            
                        // Get all cards for all lists simultaneously by calling callback3 function
            
                        Object.entries(listsData).forEach(([board, lists]) => {
                            lists.forEach((list) => {
                                callback3(list.id, (err, lists) => {
                                    if(err) {
                                        console.error(err);
                                    } else {
                                        console.log(lists);
                                    }
                                });
                            });
                        });
                    }
                });
            }
        });

    }, (Math.floor(Math.random() * 5) + 1) * 1000);
}


module.exports = callback6;
